package se.miun.dsv.jee.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "SEEDS")
@XmlRootElement
public class Seed implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@XmlElement
	private int id;

	public Seed() {
	}

	@Override
	public String toString() {
		return "Seed[" + hashCode() + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}