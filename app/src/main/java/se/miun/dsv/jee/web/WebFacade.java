package se.miun.dsv.jee.web;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se.miun.dsv.jee.model.Watermelon;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@ApplicationScoped
@Path("/")
public class WebFacade {

	@PersistenceUnit(name = "example")
	EntityManagerFactory emf;

	@GET
	@Path("hello/")
	public Hello getAddressesFor(@QueryParam("name") String name) {
		return new Hello(name);
	}

	@PUT
	@Path("watermelon/{seeds}")
	public Response initDatabase(@PathParam("seeds") int seeds) {
		if (seeds > 999)
			return Response.ok("OK, so who is going to buy a melon with thousands of seeds?").build();

		Watermelon melon = new Watermelon(seeds);

		EntityManager manager = emf.createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		try {

			tx.begin();
			manager.persist(melon);
			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
			return Response.serverError()
					.entity("An internal error occured, and however much you'd wish to know what exactly happened, we won't let you know.")
					.build();
		} finally {
			manager.close();
		}

		return Response.accepted().entity(melon).build();
	}
}