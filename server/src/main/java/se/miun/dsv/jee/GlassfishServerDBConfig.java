package se.miun.dsv.jee;

public class GlassfishServerDBConfig {

	private final String userName;

	private final String password;

	private final String id;

	private final String ip;
	
	public GlassfishServerDBConfig(String id, String userName, String password, String ip) {
		this. id = id;
		this.ip = ip;
		this.userName = userName;
		this.password = password;
	}
	
	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getConnectionPoolId() {
		return id;
	}
	
	public String getJDBCResourceId(){
		return "jdbc/" + id;
	}

	public String getIP() {
		return ip;
	}
}